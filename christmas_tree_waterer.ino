#include "Ticker.h"

#define SENSOR PIN_A0
#define PUMP 9 // SD2 on the nodeMCU
#define BUTTON_PIN 10// SD3 on the nodeMCU
#define LED_PIN 13 // D7 on the nodeMCU
#define SENSOR_ENABLE 14 // D5 on the nodeMCU
#define LOW_THRESHOLD 150
#define HIGH_THRESHOLD 400
#define READ_DELAY 5 // time between taking measurements

bool needs_water = false;
bool should_run_pump = false;

int waterLevel = 0;

void measureWaterLevel() {
  if (!should_run_pump) { // skip if the pump is running
    digitalWrite(SENSOR_ENABLE, HIGH);
    waterLevel = analogRead(SENSOR);
    digitalWrite(SENSOR_ENABLE, LOW);
    Serial.println(waterLevel);
  }
}

Ticker measureWater;

void setup() {
  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  // for some reason HIGH turns the built-in led off ¯\_(ツ)_/¯
  digitalWrite(LED_BUILTIN, HIGH); 
  
  pinMode(PUMP, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP); 
  pinMode(LED_PIN, OUTPUT);
  pinMode(SENSOR_ENABLE, OUTPUT);

  measureWater.attach(READ_DELAY, measureWaterLevel);
}

void loop() {
  if (waterLevel < LOW_THRESHOLD) {
    needs_water = true;
  }
  if (waterLevel >= HIGH_THRESHOLD) {
    needs_water = false;
    should_run_pump = false;
  }
  if (needs_water) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
  if (digitalRead(BUTTON_PIN) == LOW && needs_water) {
    should_run_pump = true;
    Serial.println("Should run pump");
  }
  if (should_run_pump) {
    digitalWrite(PUMP, HIGH);
    // want to constantly check water level if the pump is on
    digitalWrite(SENSOR_ENABLE, HIGH);
    waterLevel = analogRead(SENSOR);
    Serial.println(waterLevel);
  } else {
    digitalWrite(PUMP, LOW);
  }
}   